"""
Beacon Aggregator Python implementation
"""
# pylint: disable=no-absolute-import

import logging
import os
import json
import sys
import requests
from flask_restplus import Resource, Api
from flask import Flask, request

logging.basicConfig(format='%(levelname)s: %(message)s')

#Constants:
APP_VERSION = "19.7.2"

APP = Flask(__name__)
API = Api(APP)
AGGREGATORS = os.environ.get('aggregators', '').split(',')
BEACONS = os.environ.get('beacons', '').split(',')

LOGGFLASK = logging.getLogger('werkzeug')
LOGGLIB = logging.getLogger('urllib3.connectionpool')
LOGGROOT = logging.getLogger()

#Temporal stuff:
KNOWN_BEACONS = ["https://test-beacon-api.ega-archive.org/",
                 "https://testv2-beacon-api.ega-archive.org/"]

#==================================================================================================#
#=========================================== FUNCTIONS ============================================#
#==================================================================================================#

def print_introduction():
    """Print a friendly introduction on the console. It can be avoided using the flag --nointr (-i).
    """
    logging.info("Ignore every output in console avobe the introducton if there's any.")
    print("\n")
    print("=========================== PyBA, BEACON AGGREGATOR API ===============================")
    print("Beacon Aggregator API, service made it by Luis Gracia (PhoxSpark), researcher student \
        \nat Barcelona Supercomputing Center, INB Life Sciences.")
    print("GitLab: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - PhoxSpark")
    print("GitHub: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - PhoxSpark")
    print("eMail : - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  luis.gracia@bsc.es")
    print("Any problem with this service, you can report it on my GitLab.")
    print("Thanks for using it and have a nice day!")
    print("====================================== %s ========================================="\
        % APP_VERSION)
    print("\n")

#==================================================================================================#

def check_arguments(arguments):
    """This function will check the introduced arguments and will return an identificable string
    for the program to know which flags were specified.

    :param arguments: Argument is a **list** of flags taken by the library sys.
    :type arguments: list
    :return: Will return the **list** of strings with the identified flags. The program will take
             this list and understand it.
    :rtype: list
    """

    valid_arguments = {"--debug" : "debug",
                       "-d" : "debug",
                       "--nointr" : "nointr",
                       "-i" : "nointr"}

    results = list()

    logging.info("Checking the given flags (if there's any)...")
    for flag in arguments:
        try:
            logging.debug("Flag %s valid, appending to the results.", flag)
            results.append(valid_arguments[flag])
        except KeyError:
            logging.debug("Flag %s invalid, it will be ignored.", flag)

    logging.info("Returning the data from the given flags...")
    return results

#==================================================================================================#

def web_parser(content):
    """Try to take the title and content of the error response from a Beacon. If there is nothing
    recogniseable, the error will be unknown.

    :param content: It takes a binary object with the content of a webpage requested with the module
                    requests.
    :type content: binary
    :return: Returns a dictionary with the content it can parse (<h1> and <p>). If it can't find
             title and content, it will return a generic error message.
    :rtype: dictionary
    """

    json_load = None

    web_content = str(content.content).replace("\\n", " ")
    web_content = web_content[:500]

    #This will parse a posible result from a not working beacon.
    if "<h1>" and "</h1>" and "<p>" and "</p>" in web_content:
        title_error = web_content[web_content.find("<h1>")+4:web_content.find("</h1>")]
        parag_error = web_content[web_content.find("<p>")+3:web_content.find("</p>")]

        logging.warning("The next messages were caught from the non working beacon:\
            \n\t\t\t\tTitle: %s\n\t\t\t\tContent: %s", title_error, parag_error)
        logging.error(title_error)
        logging.error(parag_error)

        json_load = {"error" : {"title" : title_error, "content" : parag_error}}

    else:
        logging.error("Beacon unavailable. Unknown error.")

        json_load = {"error" : {"title" : "Unknown Error", "content" : "Error not recognised."}}

    return json_load

#==================================================================================================#

def endpoint_redirect(full_query_string):
    """This function will redirect every endpoint trough other known services registered.

    :param full_query_string: It will take the full parsed endpoint from Flask RESTPlus in form of
                              string.
    :type full_query_string: string
    :return: It will return a **dictionary** with the response of the query. It can handle errors on
             the listed beacons and append the error to the full result.
    :rtype: dictionary
    """

    logging.info("Starting endpoint redirecter...")

    json_load = None
    result_json = dict()
    errors = 0

    logging.info("Quering the endpoints to each known beacon...")
    for url in KNOWN_BEACONS:

        logging.debug("Making the full URL with the endpoint...")
        url_complete = url + full_query_string

        logging.debug("URL %s created.", url_complete)

        logging.debug("Requesting JSON to known beacon.")
        json_tmp = requests.get(url_complete)

        #If the service isn't working it can't get the JSON so it will crash. We need to handle it.
        try:
            logging.debug("Trying to load JSON from the requested Beacon.")
            json_load = json_tmp.json()
            logging.info("JSON from '%s' loaded successfuly.", url)

        # This exception also can try to find some text on the error page to give a reason.
        except json.decoder.JSONDecodeError:
            logging.error("Failed to load JSON from '%s'!", url)
            errors += 1

            json_load = web_parser(json_tmp)

            logging.info("The exception is handled and the service will continue.")

        logging.debug("Adding the data to a final dictionary...")
        result_json[url] = json_load

    logging.info("All the querys were maded with %i errors, returning data...", errors)
    return result_json

#==================================================================================================#

def endpoint_parser(endpoint, query_string):
    """This simple function will remove the unnecesary characters from the "request.query_string"
    function of Flask and will prepare the endpoint + querys to redirect it to it's known beacons.

    :param endpoint: It takes an endpoint without slash (/).
    :type endpoint: string
    :param query_string: It takes the query of an endpoint without interrogation (?).
    :type query_string: string
    :return: It will return the full endpoint with querys to use it on a request to the known
             beacons.
    :rtype: string
    """

    logging.info("Starting endpoint parser...")

    full_query_string = "/" + endpoint + "?" + query_string[2:]

    logging.info("Done! Returning parsed endpoint '%s'.", full_query_string)
    return full_query_string[:-1]

#==================================================================================================#
#========================================== ENDPOINTS =============================================#
#==================================================================================================#

@API.route("/<endpoint>")
class EndpointCapturer(Resource):
    """This endpoint will redirect every unidentified endpoint to all the known beacons to capture
    all their output.

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    logging.info("Endpoint requested not recognised.")

    def get(self, endpoint): # pylint: disable=no-self-use
        """GET on not recognised/registered/programmed endpoint.

        :return: It will return the API Rest result, if everything goes well, it will be in JSON
                 format.
        :rtype: dictionary (JSON)
        """

        logging.info("GET to endpoint not recognised, it will be redirected to known beacons.")

        result_json = None

        logging.debug("Parsing endpoint and getting the full query string...")
        full_query_string = endpoint_parser(endpoint, str(request.query_string))

        logging.debug("Checking if there are at least one beacon...")
        if len(KNOWN_BEACONS) >= 1:
            logging.debug("Calling the function to redirect the given endpoint...")
            result_json = endpoint_redirect(full_query_string)

        logging.info("Success! Returning JSON results...")
        return result_json

#==================================================================================================#

@API.route("/service_types", endpoint="service_types")
class ServiceTypes(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """Enumerator of known service types:
        GA4GHRegistry
        GA4GHBeacon
        GA4GHBeaconAggregator

        :return: [description]
        :rtype: dictionary (JSON)
        """
        service_types_get_response = {}
        return service_types_get_response

#==================================================================================================#

@API.route("/info", endpoint="info")
class Info(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """Returns the ServiceInfo of the service.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        info_get_response = {}
        return info_get_response

#==================================================================================================#
@API.route("/service-info", endpoint="service-info")
class ServiceInfo(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """Returns the ServiceInfoGA4GH of the service. This is equivalent to GET
        /info?model=GA4GH-ServiceInfo-v0.1

        :return: [description]
        :rtype: dictionary (JSON)
        """
        service_info_get_response = {}
        return service_info_get_response

#==================================================================================================#

@API.route("/services", endpoint="services")
class Services(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self):
        """Lists services known by this service. Returns an array of ServiceInfo.
        ¡This is the only endpoint with querys!

        /services?serviceType={serviceType}
        Returns an array of ServiceInfo filtered by type

        /services?model={model}
        Returns an array of ServiceInfo in an specific model, i.e.:
        "Beacon-v1" or "GA4GH-ServiceInfo-v0.1"

        /services?listFormat='full|short'
        full: (default) Returns an array of ServiceInfo short: returns just the id, name,
        serviceURL, ServiceType and open.

        /services?apiVersion={version}
        Returns an array of ServiceInfo filtered by Service API version supported

        :return: [description]
        :rtype: dictionary (JSON)
        """
        services_get_response = {}
        return services_get_response

    def post(self): # pylint: disable=no-self-use
        """Requires HTTPS. Including Beacon info (/datasets too ??)

        :return: [description]
        :rtype: dictionary (JSON)
        """
        services_post_response = {}
        return services_post_response

#==================================================================================================#

@API.route("/services/<string:id>", endpoint="services_id")
class ServicesID(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """List a service details. Returns the ServiceInfo of the node.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        servicesid_get_response = {}
        return servicesid_get_response

    def put(self): # pylint: disable=no-self-use
        """Requires HTTPS.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        servicesid_put_response = {}
        return servicesid_put_response

    def delete(self): # pylint: disable=no-self-use
        """Requires HTTPS.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        servicesid_delete_response = {}
        return servicesid_delete_response

#==================================================================================================#

@API.route("/registered_beacons", endpoint="registered_beacons")
class RegisteredBeacons(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def put(self): # pylint: disable=no-self-use
        """Update the list of beacons in a BA. Requires HTTPS. Used for Scenario 1d.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        registeredbeacons_put_response = {}
        return registeredbeacons_put_response

#==================================================================================================#

@API.route("/networks", endpoint="networks")
class Networks(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """All the networks info where this services is registered in.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        networks_get_response = {}
        return networks_get_response

#==================================================================================================#

@API.route("/networks/<string:id>", endpoint="networks_id")
class NetworksID(Resource):
    """[summary]

    :param Resource: Represent the abstract RESTPlus Resource.
    :type Resource: RESTPlus
    """
    def get(self): # pylint: disable=no-self-use
        """The info of this specific network where this service is registered in.

        :return: [description]
        :rtype: dictionary (JSON)
        """
        networksid_get_response = {}
        return networksid_get_response

#==================================================================================================#
#==================================================================================================#

if __name__ == "__main__":

    #Disable/enable some loggers
    APP.logger.disabled = True
    LOGGFLASK.disabled = False
    LOGGLIB.disabled = False
    LOGGROOT.disabled = False

    ARGV_RESULTS = check_arguments(sys.argv)

    if "nointr" not in ARGV_RESULTS:
        print_introduction()

    # This has to be the last one because Flask overlapse everything elese.
    if "debug" in ARGV_RESULTS:
        #Initialize Flask and check for specified flag.
        #Flag debug will enable Flask debug and debug loggings.
        LOGGROOT.setLevel(logging.DEBUG)
        LOGGLIB.setLevel(logging.DEBUG)
        LOGGFLASK.setLevel(logging.DEBUG)
        APP.run(host='0.0.0.0', port='5000', debug=True)

    else:
        #No flags will start the services with less loggings and no debug mode.
        LOGGROOT.setLevel(logging.INFO)
        LOGGLIB.setLevel(logging.INFO)
        LOGGFLASK.setLevel(logging.INFO)
        APP.run(host='0.0.0.0', port='5000')
