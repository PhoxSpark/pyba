"""
Beacon dummy Python main file
"""
# pylint: disable=no-absolute-import

import logging
import os
import sys
import flask_restplus
from flask import Flask

APP = Flask(__name__)
API = flask_restplus.Api(APP)
AGGREGATORS = os.environ.get('aggregators', '').split(',')
BEACONS = os.environ.get('beacons', '').split(',')

@API.route("/greeting")
class Greeting(flask_restplus.Resource):
    """
    Root endpoint, also, it is the Greeting Endpoint.
    """
    logging.debug("Greeting endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        GET for the Greeting endpoint
        """
        logging.debug("GET on Greeting")
        return {"Greetings":"GA4GH Beacon API"}

@API.route("/service_types")
class ServiceTypes(flask_restplus.Resource):
    """
    Service Types endpoint
    """
    logging.debug("Service Types endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        Enumerator of known service types:
        GA4GHRegistry
        GA4GHBeacon
        GA4GHBeaconAggregator
        """
        logging.debug("GET on Service Types")

@API.route("/info")
class Info(flask_restplus.Resource):
    """
    Info endpoint
    """
    logging.debug("Info endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        Returns the ServiceInfo of the service.
        """
        logging.debug("GET on Info")

@API.route("/services")
class Services(flask_restplus.Resource):
    """
    Services endpoint
    """
    logging.debug("Services endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        Lists services known by this service.
        Returns an array of ServiceInfo.
        """
        logging.debug("GET on Services")

    def post(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        Including Beacon info (/datasets too ??)
        """
        logging.debug("POST on Services")

    def put(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        """
        logging.debug("PUT on Services")

    def delete(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        """
        logging.debug("DELETE on Services")

@API.route("/registered_beacons")
class RegisteredBeacons(flask_restplus.Resource):
    """
    Registered Beacons endpoint
    """
    logging.debug("Registered Beacons endpoint")
    def put(self): # pylint: disable=no-self-use
        """
        Update the list of beacons in a BA.
        Requires HTTPS.
        Used for Scenario 1d.
        """
        logging.debug("PUT on Registered Beacons")

@API.route("/networks")
class Networks(flask_restplus.Resource):
    """
    Networks endpoint
    """
    logging.debug("Networks endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        All the networks info where this services is registered in.
        """
        logging.debug("GET on Networks")



if __name__ == "__main__":

    APP.logger.disabled = True
    LOG = logging.getLogger('werkzeug')
    LOG.disabled = True

    if "--debug" in sys.argv:
        APP.run(host='0.0.0.0', port='5000', debug=True)
        logging.basicConfig(level=logging.DEBUG)
    else:
        APP.run(host='0.0.0.0', port='5000')
        logging.basicConfig(level=logging.ERROR)
