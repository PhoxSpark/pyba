"""
Registry dummy Python main file
"""

# pylint: disable=no-absolute-import

import logging
import os
import sys
from flask_restplus import Api, Resource, reqparse
from flask import Flask

APP = Flask(__name__)
API = Api(APP)
PARSER = reqparse.RequestParser()
AGGREGATORS = os.environ.get('aggregators', '').split(',')
BEACONS = os.environ.get('beacons', '').split(',')

@API.route("/greeting", endpoint="greeting")
class Greeting(Resource):
    """
    Testing service endpoint, it has no other pourpose
    """

    logging.debug("Greeting endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        GET for the Greeting endpoint
        """

        logging.debug("GET on Greeting")
        return {"Greetings":"GA4GH Beacon Registry API"}

@API.route("/service_types", endpoint="service_types")
class ServiceTypes(Resource):
    """
    Service Types endpoint
    """

    logging.debug("Service Types endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        Enumerator of known service types:
        GA4GHRegistry
        GA4GHBeacon
        GA4GHBeaconAggregator
        """

        logging.debug("GET on Service Types")

@API.route("/info", endpoint="info")
class Info(Resource):
    """
    Info endpoint
    """

    logging.debug("Info endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        Returns the ServiceInfo of the service.
        """

        logging.debug("GET on Info")

@API.route("/services", endpoint="services")
class Services(Resource):
    """
    Services endpoint
    It has some querys to specify but they're not necessary
    """
    logging.debug("Services endpoint")

    # Query parser section, every query will save on a dictionary in PARSER.parse_args()
    logging.debug("Parsing query data...")
    PARSER.add_argument('serviceType', type=str)
    PARSER.add_argument('model', type=str)
    PARSER.add_argument('listFormat', type=str)
    PARSER.add_argument('apiVersion', type=str)

    logging.debug("Looking for methods...")
    @API.expect(PARSER)
    def get(self): # pylint: disable=no-self-use
        """
        Lists services known by this service.
        Returns an array of ServiceInfo.
        """
        logging.debug("GET on Services")

        #Get the query data into a dictionary
        parsed_dict = PARSER.parse_args()

        if parsed_dict["serviceType"] is not None:
            pass

        if parsed_dict["model"] is not None:
            pass

        if parsed_dict["listFormat"] is not None:
            pass

        if parsed_dict["apiVersion"] is not None:
            pass

    def post(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        Including Beacon info (/datasets too ??)
        """
        logging.debug("POST on Services")

    def put(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        """
        logging.debug("PUT on Services")

    def delete(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        """
        logging.debug("DELETE on Services")

@API.route("/services/<string:id>", endpoint="services_id")
class ServicesID(Resource):
    """
    Services ID endpoint
    """

    logging.debug("Services ID endpoint")

    def get(self): # pylint: disable=no-self-use
        """
        List a service details.
        Returns the ServiceInfo of the node
        """

        logging.debug("GET on Services ID")

    def put(self): # pylint: disable=no-self-use
        """
        Requires HTTPS.
        """

        logging.debug("PUT on Services ID")

@API.route("/registered_beacons", endpoint="registered_beacons")
class RegisteredBeacons(Resource):
    """
    Registered Beacons endpoint
    """

    logging.debug("Registered Beacons endpoint")
    def put(self): # pylint: disable=no-self-use
        """
        Update the list of beacons in a BA.
        Requires HTTPS.
        Used for Scenario 1d.
        """

        logging.debug("PUT on Registered Beacons")

@API.route("/networks", endpoint="networks")
class Networks(Resource):
    """
    Networks endpoint
    """
    logging.debug("Networks endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        All the networks info where this services is registered in.
        """
        logging.debug("GET on Networks")

    def post(self): # pylint: disable=no-self-use
        """
        Requires a NetworkInfo: id, name, description, organization.
        Networks are created in registries.
        BAs and Bs are registered in these networks (see next endpoint).

        """
        logging.debug("POST on Networks")

@API.route("/networks/<string:id>", endpoint="networks_id")
class NetworksID(Resource):
    """
    Networks ID endpoint
    """

    logging.debug("Networks ID endpoint")
    def get(self): # pylint: disable=no-self-use
        """
        The info of this specific network where this service is registered in.
        """

@API.route("/networks/<string:id>/services")
class NetworksIDServices(Resource):
    """
    Networks ID Services endpoint
    """

    logging.debug("Networks ID Services endpoint")
    def post(self): # pylint: disable=no-self-use
        """
        Registers a new service in this registry and network.
        Requires a ServiceInfo:  id, name, serviceURL, ServiceType, open and entryPoint.
        """

        logging.debug("POST on Networks ID Services endpoint")


        logging.debug("GET on Networks ID")

if __name__ == "__main__":

    APP.logger.disabled = True
    LOG = logging.getLogger('werkzeug')
    LOG.disabled = True

    if "--debug" in sys.argv:
        APP.run(host='0.0.0.0', port='5000', debug=True)
        logging.basicConfig(level=logging.DEBUG)
    else:
        APP.run(host='0.0.0.0', port='5000')
        logging.basicConfig(level=logging.ERROR)
