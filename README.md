# PyBA

Beacon Aggregator made by the INB - Life Sciences implemented with Python. Thir Beacon Aggregator (BA) will take it's own registry or consult the known registry services for get a list of Beacons to consult. The BA will redirect every unknown endpoint to all the Beacons, collecting every answer and showing it to the client/user.

## Project migration

The project has been migrated to this URL: https://gitlab.bsc.es/inb/ga4gh/beacon-network-demo/tree/PyBA Everything is on the branch PyBA of that repository, feel free to come there for take a look into the code. 

###**There will be no more updates or commits on this repository! Go to the BSC GitLab to see the latest changes!**