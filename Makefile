####################################################################
################# Beacon Aggregator Playground #####################
####################################################################
# This is the makefile for automatization of the playground deploy.
# At the end, the only file that matters will be the Python 
# beacon-aggregator.py, 

build:
	docker-compose -f network/docker-compose.yml up
	make clear

debug:
	docker-compose -f network-debug/docker-compose-debug.yml up
	make clear

clear-containers:
	docker rm r1.bn.com -f
	docker rm b1.bn.com -f
	docker rm b2.bn.com -f
	docker rm ba1.bn.com -f

clear-containers-debug:
	docker rm r1.dev.bn.com -f
	docker rm b1.dev.bn.com -f
	docker rm b2.dev.bn.com -f
	docker rm ba1.dev.bn.com -f


