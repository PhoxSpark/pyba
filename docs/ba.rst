ba package
==========

BA is the main module of the Beacon Aggregator. This service will ask the registries for known
beacons and save them internaly. After having a list of beacons, the BA will be able to redirect
endpoints that the BA don't know to the known beacons, in this way, the BA don't have to know the
endpoints of the beacons and can work more independently.

Also, the BA has to be able to handle any beacon error, including the connection ones or even
syntax ones. In case of beacon error, it will try to get the reason from the error page and
add it to the JSON response. In case the error can't be recognised, the JSON will include in
these faulting beacons response an error message.

Submodules
----------

ba.main module
--------------

.. automodule:: ba.main
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: ba
   :members:
   :undoc-members:
   :show-inheritance:
