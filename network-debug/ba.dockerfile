#Using Python 3.7
FROM python:3.7-alpine3.9

LABEL mantainer "BSC Developers"

RUN mkdir /service
RUN mkdir /service/imports
RUN mkdir /service/data
RUN mkdir /service/source

COPY requirements.txt /service/imports

RUN pip install --upgrade pip
RUN pip install -r /service/imports/requirements.txt -U

WORKDIR /service

CMD ["/service/source/ba/main.py", "--debug"]

ENTRYPOINT ["python"]
